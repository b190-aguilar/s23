// console.log("Hello World!");


// Trainer Object
let trainer = {
	name: "Ash Ketchum",
	age: 10,
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	pokemon: ["Pikachu", "Charizard", 'Squirtle', 'Bulbasaur'],
	talk: function(choice){
		let checkPokemon = this.pokemon.indexOf(choice);
		// console.log(checkPokemon);
		if (checkPokemon === -1){
			console.log(`${this.name} doesn't own a ${choice}`);
		}
		else{
		console.log(`${this.pokemon[checkPokemon]}! I choose you!`);
		}
	}
}
console.log(trainer);

console.log(`Result of dot notation: `);
console.log(trainer.name);
console.log(`Result of bracket notation: `);
console.log(trainer.pokemon);
console.log(`Result of talk method: `);
trainer.talk('Pikachu');
// trainer.talk(prompt("Choose your pokemon: ")); //user prompt for pokemon choice.


// Pokemon function
function Pokemon(name,level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 5 * level;
	this.attack = 3 * level;

	// Methods
	this.tackle = function(target){

		console.log(`${this.name} tackled ${target.name} for ${this.attack}.`);
		console.log(`${target.name}'s health was ${target.health}.`);
		target.health = target.health - this.attack;
		console.log(`${target.name}'s health is now reduced to ${target.health}.`);

		if (target.health <= 0){
			target.faint();
		}


	};
	this.faint = function(){
		console.log(`${this.name} fainted.`);
	}
}


let raichu = new Pokemon ("Raichu", 25);
let hitmonlee = new Pokemon ("Hitmonlee", 20);
let raticate = new Pokemon ("Raticate", 21);
let hitmonchan = new Pokemon ("Hitmonchan", 21);
let magikarp = new Pokemon ("Magikarp", 30);
let togepi = new Pokemon ("Togepi", 33);
let pikachu = new Pokemon ("Pikachu", 27);
let squirtle = new Pokemon ("Squirtle", 22);
let charizard = new Pokemon ("Charizard", 35);
let bulbasaur = new Pokemon ("Bulbasaur", 20);


let allPokemon = [raichu, hitmonlee, raticate, hitmonchan, magikarp, togepi, pikachu, charizard, squirtle, bulbasaur];

console.log(allPokemon);
