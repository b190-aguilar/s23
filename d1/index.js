console.log("Hello World");

// SECTION - Objects
/*
	- Objects are data types that are used to represent real world objects	
	- Collection of related data and/of functionalities




	- objects are commonly initialized or declared using the let + objectName and this object value will start with {}
	that is also called "Object Literals"

	SYNTAX:
		let/const objectName = {
		keyA: valueA;
		keyB: valueB;
		}
*/
// Creating using initializers
let cellphone = {
	name: "Nokie 3210",
	manufacturedDate: 1999
}

console.log("Result from creating objects using initializers: ")
console.log(cellphone);
console.log(typeof cellphone);


/*
	MINIACTIVITY
		create an object named "car"
			name/model
			release date

*/

/*
let car = {
	model: "Toyota Raize",
	releaseDate: 2022
}

console.log("Result from creating objects using initializers: ")
console.log(car);
console.log(typeof car);
*/
// creating objects using constructor function
/*
	- creates a reusable function to create several objects that have the same data structure
	- this is useful for creating multiple instances/copies an object
	- instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it.

	SYNTAX:
		ObjectName(keyA,keyB){
			this.keyA = keyA,
			this.keyB = keyB
		}
*/
/*
	'this' keyword allows us to assign a new property for the object by associating them with values received from a constructor function's parameters.
*/
function Laptop(name, manufacturedDate) {
	this.name = name;
	this.manufacturedDate = manufacturedDate;
}

// this is a unique instance/copy of the Laptop object
/*
	-'new' keyword creates an instance of an object.
	- objects and instances are often interchanged because object literals and instances are distinct/unique
*/
// let laptop =  Laptop("Dell", 2012); - not using "new" keyword would return undefined if we try to log the instance in the console since we dont have return statement in the Laptop function.
let laptop = new Laptop("Dell", 2012);
console.log("Result from creating objects using constructor functions: ")
console.log(laptop);
console.log(typeof laptop);

// creates a new Laptop object
let laptop2 = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using constructor functions: ")
console.log(laptop2);
console.log(typeof laptop2);

// creating empty objects
let computer = {};
let myComputer = new Object();
console.log("Result from creating objects using constructor functions: ")
console.log(myComputer);
console.log(typeof myComputer);



// accessing objects
/*
	in arrays, we can access the elements inside through the use of [] notation.
		arrayName[index]

	for objects, 


*/


// 	using dot notation

console.log("Result from dot notation: " + laptop.name);
console.log("Result from dot notation: " + laptop2.name);

// array of objects
/*
	- accessing arrays would have the priority since that is the first data type that we have to access
	- since we use [] to access, we will use laptops[1] to access laptop2 element inside the laptops array
	- we have to use laptops[1].name to access laptop2.name
*/
let laptops = [laptop, laptop2];

console.log(laptops[0].name);

console.log(laptops[0].manufacturedDate);
// same data pull, different notation
console.log(laptops[0]["manufacturedDate"]);

console.log(laptops[1].name);
console.log(laptops[1].manufacturedDate);
console.log(laptops[1]["manufacturedDate"]);



// SECTION - initializing, adding, deleting and reassigning object properties
/*
	like any other var in JS, objects may have their properties updated after the object was initiated.
	this is useful when an object's properties are undetermined at the time of creation.		
*/

let car = {};
console.log(car);

car.name = "Toyota Veloz"
car.manufacturedDate = 2022;
console.log(car);


// to add a new property for an object
car["manufactured date"] = 2019;
console.log(car);

/*
	while using [] will give the same feature as using dot notation, it might lean us to create unconventional naming for the properties which might lead to confusion when we try to access them.
*/

// deleting properties

delete car["manufactured date"];
console.log(car);


// reassigning properties
/*
	
*/

car.name = "Dodge Charger R/T";
console.log(car);


// Object methods
/*
	- an object method is a function that is set by the dev 	to be the value of one of the properties 
	- they are also functions and one of the differences they have is that methods are fxns related to a specific object
	- these are useful for creating object-specific fxns which are used to perform tasks on them.
	- similar to fxns/features of real world objects, methods are defined based on what an object is capable of doing and how they should do it.
*/

let person = {
	name: "John",
	talk: function(){
		console.log("Hello! My name is " + this.name)
	},

}

console.log(person);
console.log("Result from object methods:");
person.talk();



/*
	MINIACTIVITY
		try to create a walk object method inside the person object
		when we call the method, it would log in the console "<name> walked 25 steps"
*/

// adding a method
	// use = instead of : when adding another method from outside of an object
person.walk = function(){
		console.log(this.name + " walked 25 steps");
		// console.log(`${this.name} walked 25 steps`); // using template literals backticks`` + ${} + string datatype
	}

person.walk();


// Nested Objects
let friend ={
	firstName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		state: 'Texas',
	},
	email: ['joe@mail.com', 'johnHandsome@mail.com'],
	introduce: function(){
		console.log(`Hello! My name is ${this.firstName} ${this.lastName}`)
	},
}


console.log(friend);  
friend.introduce();


/*
	Scenario
		- we would like to create a game that would have several pokemon interact with each other
		- every pokemon would have the same set of stats, properties and functions.
*/

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log(`This Pokemon tackled targetPokemon`);
		console.log(`targetPokemon's health is now reduced to _targetPokemonHealth_`);
	},
	faint: function(){
		console.log("Pokemon fainted.")
	}
}

console.log(myPokemon);


// using constructor function
function Pokemon(name,level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(`targetPokemon's health is now reduced to _targetPokemonHealth_`);
	};
	this.faint = function(){
		console.log(`${this.name} fainted`);
	}
}

let hitmonlee = new Pokemon ("Hitmonlee", 20);
let raticate = new Pokemon ("Raticate", 21);

// using the tackle method from hitmonlee with raticate as its target
hitmonlee.tackle(raticate);